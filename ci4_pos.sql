-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2023 at 09:39 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci4_pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2021_01_06_035346_create_sessions_table', 1),
(7, '2021_01_17_161413_create_products_table', 1),
(8, '2021_01_17_161417_create_product_categories_table', 1),
(9, '2021_01_17_161427_create_product_galleries_table', 1),
(10, '2021_01_17_161441_create_transactions_table', 1),
(11, '2021_01_18_014436_add_roles_and_username_to_users_table', 1),
(12, '2021_03_10_095130_create_transaction_items_table', 1),
(13, '2023_03_08_032038_change_users_phone_tables', 2),
(14, '2023_03_08_091558_change_default_image_on_users', 3),
(15, '2023_03_08_093633_add_image_on_users', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'authToken', '3bb80e46ed58cbe06b04a6329b8bf1dbebb728dc9d09a9dcdfe93a2b9dec89bd', '[\"*\"]', NULL, '2023-02-24 20:58:38', '2023-02-24 20:58:38'),
(3, 'App\\Models\\User', 2, 'authToken', '60dc09e1d551e852e7e8e48605a0352667b637af1b6ce209d322e99e5b5b19c5', '[\"*\"]', NULL, '2023-02-24 21:29:31', '2023-02-24 21:29:31'),
(4, 'App\\Models\\User', 1, 'authToken', '43607ea75217c0b29ffcf4a9fa178fbf56976b84f7dd3b984cbf0906cfcb3165', '[\"*\"]', '2023-02-24 23:29:09', '2023-02-24 21:53:31', '2023-02-24 23:29:09'),
(5, 'App\\Models\\User', 3, 'authToken', '283d5b87b4ee964397f0b12a1090f6389514fcb7d220f3276a5f1b85f0034a9b', '[\"*\"]', NULL, '2023-03-07 19:00:44', '2023-03-07 19:00:44'),
(6, 'App\\Models\\User', 4, 'authToken', '91f6a2a060ab19d9e21962c4dff3f371572b0b09287cbd7f8207b462f5de0458', '[\"*\"]', NULL, '2023-03-07 20:23:45', '2023-03-07 20:23:45'),
(7, 'App\\Models\\User', 5, 'authToken', '80bd6a540d79e2f5513211f910fbd4109048cda0ae42f05a4e91575eda44aae2', '[\"*\"]', NULL, '2023-03-07 20:34:13', '2023-03-07 20:34:13'),
(8, 'App\\Models\\User', 4, 'authToken', '776a9aece127eddb84df8424736fddf12e39913824034a3a248c7c54ae44649f', '[\"*\"]', NULL, '2023-03-07 21:06:05', '2023-03-07 21:06:05'),
(9, 'App\\Models\\User', 4, 'authToken', 'cb8378be7a5cb362af919fc51309ae4cfc65e5f6d40e06bf21d4f2ce40b1ea95', '[\"*\"]', NULL, '2023-03-07 21:32:40', '2023-03-07 21:32:40'),
(10, 'App\\Models\\User', 4, 'authToken', '9421c0e2068c22b9563337b87577eda2254545f1d73e1493d7d6a5df51bec2a4', '[\"*\"]', NULL, '2023-03-08 00:15:42', '2023-03-08 00:15:42'),
(11, 'App\\Models\\User', 4, 'authToken', '8bd730bd0451f3f1928d50e17f6e72888942d0fbe7bdd9e4de926825cbc9fcb3', '[\"*\"]', NULL, '2023-03-08 00:32:35', '2023-03-08 00:32:35'),
(12, 'App\\Models\\User', 4, 'authToken', '2bbd6ca495d22cb77c76528cbfc625fbccd4a25cc6412b2f6a1a2099bce5f250', '[\"*\"]', NULL, '2023-03-08 00:36:02', '2023-03-08 00:36:02'),
(13, 'App\\Models\\User', 4, 'authToken', '8c2af18d3969240cea15d57c970755deb00b0e732626433afbc42f50f65b6179', '[\"*\"]', NULL, '2023-03-08 00:37:56', '2023-03-08 00:37:56'),
(14, 'App\\Models\\User', 4, 'authToken', '654cadb0c7cc5842d6314512bf1a2ef3d462fa4114cfe5f9c25460a7e8465dab', '[\"*\"]', NULL, '2023-03-08 00:49:56', '2023-03-08 00:49:56'),
(15, 'App\\Models\\User', 4, 'authToken', 'c42a1b626a1c26c034f9c0beb547c270044579318bf024765461c388bf5fe783', '[\"*\"]', NULL, '2023-03-08 00:52:32', '2023-03-08 00:52:32'),
(16, 'App\\Models\\User', 4, 'authToken', '8bc8a9df3c0fa1e9111d530be0754b5deb1d30aa384d5426d6d0b51f06e9f9df', '[\"*\"]', NULL, '2023-03-08 01:06:04', '2023-03-08 01:06:04'),
(17, 'App\\Models\\User', 4, 'authToken', '395ce8fd40c4d7a7049db85602648003506ec1b664d333d4f51d09b1d6c03942', '[\"*\"]', NULL, '2023-03-08 01:14:15', '2023-03-08 01:14:15'),
(18, 'App\\Models\\User', 4, 'authToken', 'c411993966ae8d59f82302cddb7599814fda94f56b9851456c8517d52d707cdb', '[\"*\"]', NULL, '2023-03-08 01:15:38', '2023-03-08 01:15:38'),
(19, 'App\\Models\\User', 4, 'authToken', '6d57a322f048235934abfe0cd651122b76a944fb07f8e801c4de2783f585c673', '[\"*\"]', NULL, '2023-03-08 01:25:43', '2023-03-08 01:25:43'),
(20, 'App\\Models\\User', 4, 'authToken', 'bdbb5defe5d8621a6b4b6a5b5ae6b6c95b0452ebfe2d58d313710d0200d3b9cb', '[\"*\"]', NULL, '2023-03-08 01:27:10', '2023-03-08 01:27:10'),
(21, 'App\\Models\\User', 4, 'authToken', '05ff39210cfe4d91f9cd5b723b91a51d7a5a93969d5368363c2292a119958cdf', '[\"*\"]', NULL, '2023-03-08 01:55:08', '2023-03-08 01:55:08'),
(22, 'App\\Models\\User', 4, 'authToken', 'b4a76a74776c8430274ac9e836e931da8d8c700bb4ebad4b89c4f5448c4ee272', '[\"*\"]', NULL, '2023-03-08 02:15:03', '2023-03-08 02:15:03'),
(23, 'App\\Models\\User', 4, 'authToken', '8616ede88ff9883b66add262cc53bab12ee598e7c708d9b59414f799295908c3', '[\"*\"]', NULL, '2023-03-08 02:25:41', '2023-03-08 02:25:41'),
(24, 'App\\Models\\User', 4, 'authToken', '49ac5c6e9354459a7395c275b0edde157f1bd0fd3ed9e7a1dd86c547f8e5fba5', '[\"*\"]', NULL, '2023-03-08 02:27:13', '2023-03-08 02:27:13'),
(25, 'App\\Models\\User', 4, 'authToken', 'c65adfc85a9c3b3e8f8b7a8c1b17d7333b61f6829b5c64ee6ba17f3027cfcfa5', '[\"*\"]', NULL, '2023-03-08 02:40:40', '2023-03-08 02:40:40'),
(26, 'App\\Models\\User', 4, 'authToken', '47c18c67c9a17b2270de0f73f09c5100314841675749f92d60bca5da9ff45236', '[\"*\"]', NULL, '2023-03-08 02:42:10', '2023-03-08 02:42:10'),
(27, 'App\\Models\\User', 4, 'authToken', '04f86676f411d70b8fcbeda928f0bee340f69bd0ab190c271c2b29a4dc54b08d', '[\"*\"]', NULL, '2023-03-08 23:41:05', '2023-03-08 23:41:05'),
(28, 'App\\Models\\User', 4, 'authToken', '4f06703e5bd06de594b77a057c53f461713a03aef644fb3cbbad5ce7838a7a83', '[\"*\"]', NULL, '2023-03-09 03:37:32', '2023-03-09 03:37:32'),
(29, 'App\\Models\\User', 4, 'authToken', 'd6765ae5e530f3cae2102c9fe53e7bbb136ca68d87498261b1f4f0dc9a1a022e', '[\"*\"]', NULL, '2023-03-09 04:08:00', '2023-03-09 04:08:00'),
(30, 'App\\Models\\User', 4, 'authToken', '75a5cc0f73b829039c5c3df6bab4399fae52a94027f991bd295f01f5729817c6', '[\"*\"]', NULL, '2023-03-09 04:11:27', '2023-03-09 04:11:27'),
(31, 'App\\Models\\User', 4, 'authToken', 'f736f3c42285b28a0d322b90281898848d0de1cf31918cf78950e4c82c2f2d75', '[\"*\"]', NULL, '2023-03-09 04:17:48', '2023-03-09 04:17:48'),
(32, 'App\\Models\\User', 4, 'authToken', '7a1d5836cefeb2a062d84c8fb72462547a6996b21fbabac5165a68298345d137', '[\"*\"]', NULL, '2023-03-09 04:22:46', '2023-03-09 04:22:46'),
(33, 'App\\Models\\User', 4, 'authToken', '280766f160397811d50738e31b06e16e37652e603a49a96dd3888d35725a2cd7', '[\"*\"]', NULL, '2023-03-09 04:27:48', '2023-03-09 04:27:48'),
(34, 'App\\Models\\User', 4, 'authToken', 'cba1aab08fa8ed2ade2e6b2a8d11bcd41201037000a09be554f6c10b05e5361c', '[\"*\"]', NULL, '2023-03-09 04:32:14', '2023-03-09 04:32:14'),
(35, 'App\\Models\\User', 4, 'authToken', '6c1f0eee5b0b50800f8e13fcc1ca12a49715f964d3ff3432c64d69c520ee7323', '[\"*\"]', NULL, '2023-03-09 04:38:49', '2023-03-09 04:38:49'),
(36, 'App\\Models\\User', 4, 'authToken', 'e46e22559e40840db7bb5803deb79edd003d5996d5f20c1361d44fc8e6371a62', '[\"*\"]', NULL, '2023-03-09 05:27:11', '2023-03-09 05:27:11'),
(37, 'App\\Models\\User', 4, 'authToken', '16ab813940ce59740e2d810812b1406f636656aa5c0000d3084b57ce9aea1377', '[\"*\"]', NULL, '2023-03-09 05:42:02', '2023-03-09 05:42:02'),
(38, 'App\\Models\\User', 4, 'authToken', 'a96a96e063531ce59c1fd700c04d4ae2f37502cfb6322c97df2ef39af1fec17f', '[\"*\"]', NULL, '2023-03-09 05:45:09', '2023-03-09 05:45:09'),
(39, 'App\\Models\\User', 4, 'authToken', 'eb41b6cfce147fb004efc1f72852d8c81eec71a7347ee6d544f98ee06e5912a4', '[\"*\"]', NULL, '2023-03-09 20:09:34', '2023-03-09 20:09:34'),
(40, 'App\\Models\\User', 4, 'authToken', 'c2e361a6c4a78b03356c28480b5da123e0245be5214aba15fea6e803e2197848', '[\"*\"]', NULL, '2023-03-09 20:13:22', '2023-03-09 20:13:22'),
(41, 'App\\Models\\User', 4, 'authToken', '834ebf26977a63d9cf101f37a7631263d0da1d9d6fc73bfb932b238874f43dbf', '[\"*\"]', NULL, '2023-03-09 20:19:38', '2023-03-09 20:19:38'),
(42, 'App\\Models\\User', 4, 'authToken', '761d01b910c51d7bc5ac43b376502309ce90f810beadedfba9289562cdfd031b', '[\"*\"]', NULL, '2023-03-09 20:22:39', '2023-03-09 20:22:39'),
(43, 'App\\Models\\User', 4, 'authToken', '04739e049a016b9b0cdefc14a2c0667436b3f05d1651b92956a2680fc107b0b6', '[\"*\"]', NULL, '2023-03-09 20:24:59', '2023-03-09 20:24:59'),
(44, 'App\\Models\\User', 4, 'authToken', '234a847457e1caf4da94af15b120ccbea3e10b766830665d11932f63255f042d', '[\"*\"]', NULL, '2023-03-09 20:45:20', '2023-03-09 20:45:20'),
(45, 'App\\Models\\User', 4, 'authToken', '26001ee81ec865e8233684afdac27851be44aadd7ca3a28be969278bad9f292e', '[\"*\"]', NULL, '2023-03-09 23:00:02', '2023-03-09 23:00:02'),
(46, 'App\\Models\\User', 4, 'authToken', '81d7225c94a7de0e3ac602b7a42c441c6c8bcf517ff53d8b45d64adf1e575741', '[\"*\"]', NULL, '2023-03-09 23:05:40', '2023-03-09 23:05:40'),
(47, 'App\\Models\\User', 4, 'authToken', '2acd58a3f63c062dbb57d60342e95738c034da1e910c3113ca0d863495ef4b6c', '[\"*\"]', '2023-03-09 23:53:50', '2023-03-09 23:37:42', '2023-03-09 23:53:50'),
(48, 'App\\Models\\User', 4, 'authToken', 'cad9bb38920b5b2a3f041a3e420796050086b5edf20949bb6b44c3588e8198bc', '[\"*\"]', NULL, '2023-03-10 00:30:32', '2023-03-10 00:30:32'),
(49, 'App\\Models\\User', 4, 'authToken', '54ea722ffcb979c05018c879de3b9eca78d4ae284cce29011400d23bf6c30d47', '[\"*\"]', NULL, '2023-03-10 00:36:38', '2023-03-10 00:36:38'),
(50, 'App\\Models\\User', 4, 'authToken', '2cc4f083131890f9265f25ab6b70dcc27c799a35b7c17377975e015694d86a43', '[\"*\"]', NULL, '2023-03-10 00:42:15', '2023-03-10 00:42:15'),
(51, 'App\\Models\\User', 4, 'authToken', '7fde9dbec062ac3244b8ca1b048b9f91d12fc1c0965d108282602724d4330641', '[\"*\"]', NULL, '2023-03-10 00:57:34', '2023-03-10 00:57:34'),
(52, 'App\\Models\\User', 4, 'authToken', '45c09436d37afc4f937618c793544e7068751e45cc632f1cc2f0ac200aad22b1', '[\"*\"]', NULL, '2023-03-10 01:00:37', '2023-03-10 01:00:37'),
(53, 'App\\Models\\User', 4, 'authToken', '2d381a90cd762d3754c759ffccf60f34c2da86ab53a48cce73cc4fe83c01a89f', '[\"*\"]', '2023-03-10 01:07:40', '2023-03-10 01:07:20', '2023-03-10 01:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories_id` bigint(20) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `tags`, `categories_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Adidas NMD', 200.00, 'Ini adalah sepatu sport', NULL, 1, NULL, '2021-04-15 02:16:06', '2021-04-15 02:16:06'),
(2, 'Ultra 4D 5 Shoes', 285.00, 'When the adidas Ultraboost debuted back\r\nin 2015, it had an impact that spilled beyond\r\nthe world of running. For this version of t...', NULL, 5, NULL, '2021-04-15 10:17:22', '2021-04-15 10:27:20'),
(3, 'SL 20 Shoes', 123.00, 'These adidas SL20 Shoes will back your play. \r\nLightweight cushioning gives you a faster \r\npush-off and a snappy feel.', NULL, 5, NULL, '2021-04-15 10:18:19', '2021-04-15 10:39:03'),
(4, 'Ultra 4D 5 Shoes', 285.00, 'When the adidas Ultraboost debuted back \r\nin 2015, it had an impact that spilled beyond \r\nthe world of running.', NULL, 5, NULL, '2021-04-15 10:18:51', '2021-04-15 10:40:20'),
(5, 'Ultraboots 20 Shoes', 206.00, 'Wear your values on your feet with these adi\r\ndas running shoes. Rocking the wild colours \r\nshows you\'re not shy about standing out.', NULL, 5, NULL, '2021-04-15 10:19:08', '2021-04-15 10:43:19'),
(6, 'LEGO® SPORT SHOES', 92.00, 'These shoes keep kids comfortable through\r\nplaytime, whether that means running around\r\non building universes out of bricks.', NULL, 4, NULL, '2021-04-15 10:20:03', '2021-05-04 07:33:37'),
(7, 'Fortarun Running Shoes 2020', 34.00, 'Whether they\'re headed to school, day care\r\nor the playground with friends, send them\r\nout in all-day foot support with these adidas.', NULL, 4, NULL, '2021-04-15 10:21:05', '2021-05-04 07:36:17'),
(8, 'Supernove Running Shoes', 83.00, 'Two kinds of cushioning in the midsole give\r\nyou flexibility and responsiveness right where\r\nyou need them.', NULL, 4, NULL, '2021-04-15 10:22:10', '2021-05-04 07:37:14'),
(9, 'Faito Summer.RDY Tokyo Shoes', 76.00, 'Whether you\'re running out the front door to\r\nlog a few miles or want to bring a running-\r\ninspired style statement to your everyday.', NULL, 4, NULL, '2021-04-15 10:22:39', '2021-05-04 07:38:53'),
(10, 'DAME 7 SHOES', 125.00, 'Show up in big game style whether you\'re trying out for the team or challenging a friend to some one-on-one in the park.', NULL, 3, NULL, '2021-05-04 07:40:05', '2021-05-04 11:52:12'),
(11, 'Pro boots low shoes', 57.00, 'The superlight midsole features an innovative\r\ndrop-in that provides outstanding energy \r\nreturn every time you plant.', NULL, 3, NULL, '2021-05-04 07:40:30', '2021-05-04 11:55:13'),
(12, 'D.O.N ISSUE 2.0 Shoes', 111.00, 'These signature shoes from Mitchell and\r\nadidas Basketball feature graphics that\r\nhighlight the young All-Star\'s style.', NULL, 3, NULL, '2021-05-04 11:35:31', '2021-05-04 11:56:00'),
(13, 'Harden Vol. 4 Shoes', 137.00, 'Most guys are finished by late in the fourth\r\nquarter. That\'s when James Harden is just\r\ngetting started.', NULL, 3, NULL, '2021-05-04 11:36:51', '2021-05-04 11:56:57'),
(14, 'Terrex urban low  Hiking Shoes', 143.00, 'Unpaved trails and mixed surfaces are easy\r\nwhen you have the traction and support you\r\nneed. Casual enough for the daily commute.', NULL, 2, NULL, '2021-05-04 11:37:22', '2021-05-04 11:59:49'),
(15, 'TERREX EASTRAIL HIKING SHOES', 54.00, 'Built for a stable feel and confident traction\r\non wet, uneven terrain. Stack up the trail\r\nmiles in these lightweight hiking shoes', NULL, 2, NULL, '2021-05-04 11:38:04', '2021-05-04 12:10:09'),
(16, 'TERREX AX3 HIKING SHOES', 83.00, 'GORE-TEX Upper Mesh and synthetic uppers\r\nwill encase each foot in durable comfort. \r\nContinental Rubber Outs', NULL, 2, NULL, '2021-05-04 11:38:29', '2021-05-04 12:14:10'),
(17, 'TERREX TRAILMAKER HIKING SHOES', 34.00, 'The adidas Terrex Trailmaker Hiking Shoes\r\nblend responsive running-shoe design with\r\ntrail-shoe support.', NULL, 2, NULL, '2021-05-04 11:38:53', '2021-05-04 12:04:09'),
(18, 'Testing Product 2023', 100.00, 'Testing Product 2023 Desc', NULL, 2, NULL, '2023-03-19 21:08:18', '2023-03-19 21:08:18'),
(19, 'Testing Product 2023 2', 99.00, 'Testing Product 2023 2', NULL, 2, NULL, '2023-03-19 21:10:43', '2023-03-19 21:10:43');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Sport', NULL, '2021-04-15 02:15:27', '2021-04-15 02:15:27'),
(2, 'Hiking', NULL, '2021-04-15 02:15:33', '2021-04-15 02:15:33'),
(3, 'Basketball', NULL, '2021-04-15 10:09:12', '2021-04-15 10:09:12'),
(4, 'Training', NULL, '2021-04-15 10:12:34', '2021-04-15 10:21:21'),
(5, 'Running', NULL, '2021-04-15 10:12:44', '2021-04-15 10:12:44'),
(6, 'All Shoes', NULL, '2021-04-15 10:14:25', '2021-04-15 10:14:25'),
(9, 'Futsal 2', NULL, '2023-03-20 21:35:20', '2023-03-20 21:35:27');

-- --------------------------------------------------------

--
-- Table structure for table `product_galleries`
--

CREATE TABLE `product_galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `products_id` bigint(20) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_galleries`
--

INSERT INTO `product_galleries` (`id`, `products_id`, `url`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'public/gallery/sepatu2.png', NULL, '2021-04-15 10:30:43', '2021-04-15 10:30:43'),
(2, 2, 'public/gallery/sepatu.png', NULL, '2021-04-15 10:30:43', '2021-04-15 10:30:43'),
(3, 2, 'public/gallery/sepatu2.png', NULL, '2021-04-15 10:30:43', '2021-04-15 10:30:43'),
(4, 2, 'public/gallery/sepatu.png', NULL, '2021-04-15 10:30:43', '2021-04-15 10:30:43'),
(5, 2, 'public/gallery/sepatu2.png', NULL, '2021-04-15 10:30:43', '2021-04-15 10:30:43'),
(6, 2, 'public/gallery/sepatu.png', NULL, '2021-04-15 10:30:43', '2021-04-15 10:30:43'),
(7, 3, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:23:36', '2021-05-04 07:23:36'),
(8, 3, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:23:36', '2021-05-04 07:23:36'),
(9, 3, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:23:36', '2021-05-04 07:23:36'),
(10, 3, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:23:36', '2021-05-04 07:23:36'),
(11, 3, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:23:36', '2021-05-04 07:23:36'),
(12, 3, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:23:36', '2021-05-04 07:23:36'),
(13, 4, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:24:38', '2021-05-04 07:24:38'),
(14, 4, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:24:38', '2021-05-04 07:24:38'),
(15, 4, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:24:38', '2021-05-04 07:24:38'),
(16, 4, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:24:38', '2021-05-04 07:24:38'),
(17, 4, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:24:38', '2021-05-04 07:24:38'),
(18, 4, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:24:38', '2021-05-04 07:24:38'),
(19, 5, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:25:28', '2021-05-04 07:25:28'),
(20, 5, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:25:28', '2021-05-04 07:25:28'),
(21, 5, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:25:28', '2021-05-04 07:25:28'),
(22, 5, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:25:28', '2021-05-04 07:25:28'),
(23, 5, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:25:28', '2021-05-04 07:25:28'),
(24, 5, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:25:28', '2021-05-04 07:25:28'),
(25, 6, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:26:32', '2021-05-04 07:26:32'),
(26, 6, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:26:32', '2021-05-04 07:26:32'),
(27, 6, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:26:32', '2021-05-04 07:26:32'),
(28, 6, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:26:32', '2021-05-04 07:26:32'),
(29, 6, 'public/gallery/sepatu2.png', '2021-05-04 07:26:39', '2021-05-04 07:26:32', '2021-05-04 07:26:39'),
(30, 6, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:26:32', '2021-05-04 07:26:32'),
(31, 6, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:26:32', '2021-05-04 07:26:32'),
(32, 7, 'public/gallery/sepatu.png', '2021-05-04 07:27:51', '2021-05-04 07:27:11', '2021-05-04 07:27:51'),
(33, 7, 'public/gallery/sepatu2.png', '2021-05-04 07:27:51', '2021-05-04 07:27:11', '2021-05-04 07:27:51'),
(34, 7, 'public/gallery/sepatu.png', '2021-05-04 07:27:56', '2021-05-04 07:27:11', '2021-05-04 07:27:56'),
(35, 7, 'public/gallery/sepatu2.png', '2021-05-04 07:27:56', '2021-05-04 07:27:11', '2021-05-04 07:27:56'),
(36, 7, 'public/gallery/sepatu.png', '2021-05-04 07:27:55', '2021-05-04 07:27:11', '2021-05-04 07:27:55'),
(37, 7, 'public/gallery/sepatu2.png', '2021-05-04 07:27:54', '2021-05-04 07:27:11', '2021-05-04 07:27:54'),
(38, 7, 'public/gallery/sepatu.png', '2021-05-04 07:29:17', '2021-05-04 07:28:09', '2021-05-04 07:29:17'),
(39, 7, 'public/gallery/sepatu2.png', '2021-05-04 07:29:18', '2021-05-04 07:28:09', '2021-05-04 07:29:18'),
(40, 7, 'public/gallery/sepatu.png', '2021-05-04 07:29:21', '2021-05-04 07:28:09', '2021-05-04 07:29:21'),
(41, 7, 'public/gallery/sepatu2.png', '2021-05-04 07:29:21', '2021-05-04 07:28:09', '2021-05-04 07:29:21'),
(42, 7, 'public/gallery/sepatu.png', '2021-05-04 07:29:20', '2021-05-04 07:28:09', '2021-05-04 07:29:20'),
(43, 7, 'public/gallery/sepatu2.png', '2021-05-04 07:29:20', '2021-05-04 07:28:09', '2021-05-04 07:29:20'),
(44, 8, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:29:36', '2021-05-04 07:29:36'),
(45, 8, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:29:36', '2021-05-04 07:29:36'),
(46, 8, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:29:36', '2021-05-04 07:29:36'),
(47, 8, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:29:36', '2021-05-04 07:29:36'),
(48, 8, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:29:36', '2021-05-04 07:29:36'),
(49, 8, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:29:36', '2021-05-04 07:29:36'),
(50, 7, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:30:16', '2021-05-04 07:30:16'),
(51, 7, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:30:16', '2021-05-04 07:30:16'),
(52, 7, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:30:16', '2021-05-04 07:30:16'),
(53, 7, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:30:16', '2021-05-04 07:30:16'),
(54, 7, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:30:16', '2021-05-04 07:30:16'),
(55, 7, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:30:16', '2021-05-04 07:30:16'),
(56, 9, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:30:53', '2021-05-04 07:30:53'),
(57, 9, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:30:53', '2021-05-04 07:30:53'),
(58, 9, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:30:53', '2021-05-04 07:30:53'),
(59, 9, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:30:53', '2021-05-04 07:30:53'),
(60, 9, 'public/gallery/sepatu.png', NULL, '2021-05-04 07:30:53', '2021-05-04 07:30:53'),
(61, 9, 'public/gallery/sepatu2.png', NULL, '2021-05-04 07:30:53', '2021-05-04 07:30:53'),
(62, 10, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:43:14', '2021-05-04 11:43:14'),
(63, 10, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:43:14', '2021-05-04 11:43:14'),
(64, 10, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:43:14', '2021-05-04 11:43:14'),
(65, 10, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:43:14', '2021-05-04 11:43:14'),
(66, 10, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:43:14', '2021-05-04 11:43:14'),
(67, 10, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:43:14', '2021-05-04 11:43:14'),
(68, 12, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:44:49', '2021-05-04 11:44:49'),
(69, 12, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:44:49', '2021-05-04 11:44:49'),
(70, 12, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:44:49', '2021-05-04 11:44:49'),
(71, 12, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:44:49', '2021-05-04 11:44:49'),
(72, 12, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:44:49', '2021-05-04 11:44:49'),
(73, 12, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:44:49', '2021-05-04 11:44:49'),
(74, 11, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:45:34', '2021-05-04 11:45:34'),
(75, 11, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:45:34', '2021-05-04 11:45:34'),
(76, 11, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:45:34', '2021-05-04 11:45:34'),
(77, 11, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:45:34', '2021-05-04 11:45:34'),
(78, 11, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:45:34', '2021-05-04 11:45:34'),
(79, 11, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:45:34', '2021-05-04 11:45:34'),
(80, 13, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:46:41', '2021-05-04 11:46:41'),
(81, 13, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:46:41', '2021-05-04 11:46:41'),
(82, 13, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:46:41', '2021-05-04 11:46:41'),
(83, 13, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:46:41', '2021-05-04 11:46:41'),
(84, 13, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:46:41', '2021-05-04 11:46:41'),
(85, 13, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:46:41', '2021-05-04 11:46:41'),
(86, 14, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:48:15', '2021-05-04 11:48:15'),
(87, 14, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:48:15', '2021-05-04 11:48:15'),
(88, 14, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:48:15', '2021-05-04 11:48:15'),
(89, 14, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:48:15', '2021-05-04 11:48:15'),
(90, 14, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:48:15', '2021-05-04 11:48:15'),
(91, 14, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:48:15', '2021-05-04 11:48:15'),
(92, 15, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:49:21', '2021-05-04 11:49:21'),
(93, 15, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:49:21', '2021-05-04 11:49:21'),
(94, 15, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:49:21', '2021-05-04 11:49:21'),
(95, 15, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:49:21', '2021-05-04 11:49:21'),
(96, 15, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:49:21', '2021-05-04 11:49:21'),
(97, 15, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:49:21', '2021-05-04 11:49:21'),
(98, 17, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:49:46', '2021-05-04 11:49:46'),
(99, 17, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:49:46', '2021-05-04 11:49:46'),
(100, 17, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:49:46', '2021-05-04 11:49:46'),
(101, 17, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:49:46', '2021-05-04 11:49:46'),
(102, 17, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:49:46', '2021-05-04 11:49:46'),
(103, 17, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:49:46', '2021-05-04 11:49:46'),
(104, 16, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:50:13', '2021-05-04 11:50:13'),
(105, 16, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:50:13', '2021-05-04 11:50:13'),
(106, 16, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:50:13', '2021-05-04 11:50:13'),
(107, 16, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:50:13', '2021-05-04 11:50:13'),
(108, 16, 'public/gallery/sepatu.png', NULL, '2021-05-04 11:50:13', '2021-05-04 11:50:13'),
(109, 16, 'public/gallery/sepatu2.png', NULL, '2021-05-04 11:50:13', '2021-05-04 11:50:13'),
(110, 19, '4.png', NULL, '2023-03-20 21:38:19', '2023-03-20 21:38:19');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('0F2WyaKjAmk2ioafMHLebv5dZUpwg0q8EnIqy1gS', NULL, '198.244.140.11', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/109.0', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiWExuSTJaMWs0QkR6Q0IxVmUzT1RITmg2bjVsYmFESE16WHJpdHVsOCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjU6Imh0dHBzOi8vd3d3LmRldmFydWwubXkuaWQiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1677824727),
('1P9oqCGCi5C3mhLNgsnOJLgBGg00YY9qOY4SMmHE', NULL, '183.129.153.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiTjJuSTR6VFNwY0ZyYURHSVVVc2JRNk1PZnRNMW1JYkpxUng4WHpFNSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjU6Imh0dHBzOi8vd3d3LmRldmFydWwubXkuaWQiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1677960354),
('2z0GEUgwFDzgbcWBjQa7iEnuzdiedxfZILCmpwq9', NULL, '65.21.206.43', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36 Edg/91.0.864.54', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoicW9JWlo4TTRDR1dGd3Vub1NhTkRodExIQnRoQ2lNVFNtYm1BUmJFWSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHBzOi8vZGV2YXJ1bC5teS5pZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1677399954),
('3x62tp1SMsAfoVhu4CO0o8cpJaGIKIyqbiK8YTmD', NULL, '120.188.64.203', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiUVRibU5KQUM4ektEN25GeFU3UlZwZG9aYWdnTmVxS3FUS25HV09NdiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHBzOi8vZGV2YXJ1bC5teS5pZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1677308364),
('56oL4QVUhDzuTutmNg2N9vzD6kV64YyiLEKqeBEz', NULL, '183.129.153.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiWlJLM1c5TnVIc3pKcjZqdWRJU2wzdzg1T0puZHlydzBhQXUzY3FOQyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjY6Imh0dHBzOi8vbWFpbC5kZXZhcnVsLm15LmlkIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1677958556),
('6Do50ZiNnIk7opYyfTljFESubKAzqZysKtKTVRmZ', NULL, '127.0.0.1', 'PostmanRuntime/7.30.0', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiVUVEMHpvMUJaSHozWWtmaWZGTk4xWW9XNnNwamgwcWpNM1NaSUtNWCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1677300135),
('AJvZBchWmwg4kBP9ObvAU3dWe9GiJ2FShJkvhY6W', NULL, '35.165.215.140', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.5359.125 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiNndpRjJiV3BtdFZvMzZNRDFQNXZ0UnVtUGN4bGgwaFpYWW83ZjNaTiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHBzOi8vZGV2YXJ1bC5teS5pZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1678846753),
('DO1G3oNcKsLhCU3PTAXsaE718sePOJk2NUUDUnHK', NULL, '34.251.241.166', 'Mozilla/5.0 (compatible; NetcraftSurveyAgent/1.0; +info@netcraft.com)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiMEFpbFZNTUdwZHAyVlNHbE9VajhBQThpdnA5Y2Znc3RubFoyT2MyYiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHBzOi8vZGV2YXJ1bC5teS5pZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1678222505),
('dPBCQcuj0zZCkTFYkv6q6Ro7qWDrGGromU7Y7Lzk', NULL, '114.10.29.152', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiR1dKUHh3WDgwREQwNVowZVlVa0JiODd0WDViOUZzcUw2UUp0RHRDSyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHBzOi8vZGV2YXJ1bC5teS5pZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1678240593),
('ecHOrdDbHZfrqKQsPDsJkSRuzNk3uhjSZ24wePQG', NULL, '103.129.220.171', '', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiU2o3NTFCYXpDclVFN2prYnRzRG5rTVAzdGhqRjZ6YzI2clBYbjJwZiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1677376177),
('EQ2ERyoW9cqpliP7EsjsCExVZaLqqGbcPm73X7L0', NULL, '103.129.220.171', '', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiWnhSYUROWTFRS0FRNVY5c1FDMmxBcGtCZXg3dnBqQUJ3VHIwbWx2RSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1677634597),
('h1ef3wTdHory4hfCFHCAQGS4Y8eEw04uP8fSgg3I', NULL, '183.129.153.157', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiRkQwaGFabGluTmg5RTNQcndEYTNJeVV2UmNkb25YdEZ1MUZ5MHh5UCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjY6Imh0dHBzOi8vbWFpbC5kZXZhcnVsLm15LmlkIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1677958555),
('HLxwft4DyjGecr3nI4kbrSu99v6j1924lv6KnIeP', NULL, '120.188.66.8', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiZXI5aWd4Q2FkWHpoRzhldG1HYVA0MHlSYVpjbWZGR1Y3a0RyS0VFcCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1678359289),
('lvaojXxmePywpVY8awFDg0pZtYkkgZEbEouCAGL8', NULL, '103.129.220.171', '', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoic1k1MEkyVzVHZGVVVDNlZWltekdvdlJrZ3JiWmNBVmVTSm9TYldSRyI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1677546095),
('Nt33mXdoL51Cwy2ePYTKqpxfISOAO6LkqNOzPnBd', NULL, '199.244.88.231', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiaWxkQWF5R2FqN2d1aW1qRWVybGI0MWxMNDFKUUFic1RlVlFCZFdYZiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHBzOi8vZGV2YXJ1bC5teS5pZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1677538186),
('tPoQ5VE6Wx74tD9UWpIY7pGr0KYXrgjHQSDU6mER', NULL, '31.13.127.8', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiZEtmSmd5dWhtZTNKZnNpeDVLOEdlS0JTR0ZIVllCMzZoMzVOQ3pSQiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6OTE6Imh0dHBzOi8vZGV2YXJ1bC5teS5pZC8/ZmJjbGlkPUl3QVIwR2Fhd0E1bU0wczBVVm0zZlhPZlkxZ2dqT2JKWFEtVl8zNkRLY3FuaWdFMzRkZ2lXa216YzZLazQiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1678674890),
('wX4lltnpsqQlqYXLl6bJ9hxZiwcOd58t5xbWWI0w', NULL, '52.49.183.166', 'Mozilla/5.0 (compatible; NetcraftSurveyAgent/1.0; +info@netcraft.com)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiZGF1STlIYzJQS0VBYjVMeXhyNGFtbFNlWThaMWFEYkI3cllBWFphdSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjU6Imh0dHBzOi8vd3d3LmRldmFydWwubXkuaWQiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1677822860),
('ZNx2iRIzEYw8u4m2fSqF2rG37H2LePffeF9Dvvdx', NULL, '109.239.58.107', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36 Edg/91.0.864.54', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiUXFYT3JqUnM0Vmt1REUwRUtWOTFpWXBVSVR4dENvdUZSdEs3UzdiWSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHBzOi8vZGV2YXJ1bC5teS5pZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1678442294);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_team` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `team_invitations`
--

CREATE TABLE `team_invitations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `team_id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `team_user`
--

CREATE TABLE `team_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `team_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` bigint(20) NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` double(8,2) NOT NULL DEFAULT 0.00,
  `shipping_price` double(8,2) NOT NULL DEFAULT 0.00,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PENDING',
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'MANUAL',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `users_id`, `address`, `total_price`, `shipping_price`, `status`, `payment`, `deleted_at`, `created_at`, `updated_at`) VALUES
(12, 4, 'Cikarang Kota', 2000.00, 2000.00, 'PAID', 'MANUAL', NULL, '2023-02-24 23:25:57', '2023-02-24 23:25:57'),
(13, 4, 'Cikarang Kota', 2000.00, 2000.00, 'PENDING', 'MANUAL', NULL, '2023-02-24 23:29:09', '2023-02-24 23:29:09'),
(14, 4, 'Marsmoon', 570.00, 0.00, 'PENDING', 'MANUAL', NULL, '2023-03-09 23:53:50', '2023-03-09 23:53:50'),
(15, 4, 'Marsmoon', 285.00, 0.00, 'PENDING', 'MANUAL', NULL, '2023-03-10 01:07:40', '2023-03-10 01:07:40'),
(16, 7, '-', 422.00, 0.00, 'PAID', 'MANUAL', NULL, '2023-03-22 16:59:34', '2023-03-22 16:59:34'),
(17, 7, '-', 622.00, 0.00, 'PAID', 'MANUAL', NULL, '2023-03-22 17:25:43', '2023-03-22 17:25:43'),
(18, 7, '-', 650.00, 0.00, 'PAID', 'MANUAL', NULL, '2023-03-22 17:27:26', '2023-03-22 17:27:26'),
(19, 7, '-', 830.00, 0.00, 'PAID', 'MANUAL', NULL, '2023-03-22 17:59:26', '2023-03-22 17:59:26'),
(20, 7, '-', 676.00, 0.00, 'PAID', 'MANUAL', NULL, '2023-03-22 18:03:59', '2023-03-22 18:03:59');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_items`
--

CREATE TABLE `transaction_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` bigint(20) NOT NULL,
  `products_id` bigint(20) NOT NULL,
  `transactions_id` bigint(20) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaction_items`
--

INSERT INTO `transaction_items` (`id`, `users_id`, `products_id`, `transactions_id`, `quantity`, `created_at`, `updated_at`) VALUES
(14, 1, 2, 12, 1, '2023-02-24 23:25:57', '2023-02-24 23:25:57'),
(15, 1, 4, 12, 1, '2023-02-24 23:25:57', '2023-02-24 23:25:57'),
(16, 1, 3, 12, 2, '2023-02-24 23:25:57', '2023-02-24 23:25:57'),
(17, 1, 2, 13, 1, '2023-02-24 23:29:09', '2023-02-24 23:29:09'),
(18, 1, 4, 13, 1, '2023-02-24 23:29:09', '2023-02-24 23:29:09'),
(19, 1, 3, 13, 2, '2023-02-24 23:29:09', '2023-02-24 23:29:09'),
(20, 4, 2, 14, 2, '2023-03-09 23:53:50', '2023-03-09 23:53:50'),
(21, 4, 2, 15, 1, '2023-03-10 01:07:40', '2023-03-10 01:07:40'),
(22, 7, 12, 16, 2, '2023-03-22 16:59:34', '2023-03-22 16:59:34'),
(23, 7, 1, 16, 1, '2023-03-22 16:59:34', '2023-03-22 16:59:34'),
(24, 7, 12, 17, 2, '2023-03-22 17:25:43', '2023-03-22 17:25:43'),
(25, 7, 1, 17, 2, '2023-03-22 17:25:43', '2023-03-22 17:25:43'),
(26, 7, 10, 18, 2, '2023-03-22 17:27:26', '2023-03-22 17:27:26'),
(27, 7, 1, 18, 2, '2023-03-22 17:27:26', '2023-03-22 17:27:26'),
(28, 7, 12, 19, 2, '2023-03-22 17:59:26', '2023-03-22 17:59:26'),
(29, 7, 9, 19, 8, '2023-03-22 17:59:26', '2023-03-22 17:59:26'),
(30, 7, 1, 20, 3, '2023-03-22 18:03:59', '2023-03-22 18:03:59'),
(31, 7, 9, 20, 1, '2023-03-22 18:03:59', '2023-03-22 18:03:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USER',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT 'https://images.unsplash.com/photo-1547721064-da6cfb341d50',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'https://images.unsplash.com/photo-1547721064-da6cfb341d50'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `username`, `roles`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`, `image`) VALUES
(4, 'Arul', 'muhamadarul907@gmail.com', NULL, 'arul', 'USER', NULL, '$2y$10$08qGg3Mr3cL7DdPrDBI.VO3aQe0iV8yycIq9bs2Y4L13krhpnM5LK', NULL, NULL, NULL, NULL, NULL, '2023-03-07 20:23:45', '2023-03-07 20:23:45', 'https://images.unsplash.com/photo-1547721064-da6cfb341d50'),
(5, 'Rizky', 'rizky@mail.con', NULL, 'rizky', 'USER', NULL, '$2y$10$J.DK3OLZqHQ3RO3M2z4FbeAhUdXCUkdBiQhQ6miZUZNoL1LYs4.OK', NULL, NULL, NULL, NULL, NULL, '2023-03-07 20:34:13', '2023-03-07 20:34:13', 'https://images.unsplash.com/photo-1547721064-da6cfb341d50'),
(7, 'Desti', 'desti@mail.com', NULL, NULL, 'USER', NULL, '$2y$10$RmKH7f24j4X9Jv3LJ9J2ZuoaKsdQ36iHUZoocbRxaM6zZ.8Xl3ftm', NULL, NULL, NULL, NULL, 'https://images.unsplash.com/photo-1547721064-da6cfb341d50', '2023-03-17 19:56:05', '2023-03-17 19:56:05', 'https://images.unsplash.com/photo-1547721064-da6cfb341d50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_galleries`
--
ALTER TABLE `product_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teams_user_id_index` (`user_id`);

--
-- Indexes for table `team_invitations`
--
ALTER TABLE `team_invitations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_invitations_team_id_email_unique` (`team_id`,`email`);

--
-- Indexes for table `team_user`
--
ALTER TABLE `team_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_user_team_id_user_id_unique` (`team_id`,`user_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_items`
--
ALTER TABLE `transaction_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `product_galleries`
--
ALTER TABLE `product_galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `team_invitations`
--
ALTER TABLE `team_invitations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `team_user`
--
ALTER TABLE `team_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `transaction_items`
--
ALTER TABLE `transaction_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `team_invitations`
--
ALTER TABLE `team_invitations`
  ADD CONSTRAINT `team_invitations_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
