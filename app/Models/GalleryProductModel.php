<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class GalleryProductModel extends Model{
    protected $table = 'product_galleries';
    protected $primaryKey = 'id';
    protected $returnType     = 'object';    
    protected $allowedFields = ['products_id','url','created_at', 'updated_at', 'deleted_at'];
    protected $useTimestamps = true;

    public function galleryWithProduct($product_id = null, $paginate = null){
        $data = $this->table('product_galleries')
        ->select('product_galleries.*, products.name as product_name')
        ->join('products', 'product_galleries.products_id = products.id' )
        ->orderBy('product_galleries.id', 'DESC');
        
        if ($product_id != null) {
          $data = $data->where('products_id', $product_id);
        }

        if ($paginate == null) {
            return $data->get();
        }else{
            return  $data->paginate($paginate, 'gallery');
        }
       
    }
}