<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class TransactionDetailModel extends Model
{
    protected $table      = 'transaction_items';
    protected $primaryKey = 'id';
    protected $returnType     = 'object';
    protected $allowedFields = ['users_id', 'products_id','transactions_id','quantity','created_at', 'updated_at'];
    protected $useTimestamps = true;

}