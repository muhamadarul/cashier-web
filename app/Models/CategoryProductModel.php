<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class CategoryProductModel extends Model
{
    protected $table      = 'product_categories';
    protected $primaryKey = 'id';
    protected $returnType     = 'object';
    protected $allowedFields = ['name','created_at', 'updated_at'];
    protected $useTimestamps = true;
}