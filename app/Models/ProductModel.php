<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class ProductModel extends Model
{
    protected $table      = 'products';
    protected $primaryKey = 'id';
    protected $returnType     = 'object';
    protected $allowedFields = ['name', 'price','categories_id','description', 'tags','created_at', 'updated_at'];
    protected $useTimestamps = true;

    public function productWithCategory($paginate = null){
        $data = $this->table('products')
        ->select('products.*, product_categories.name as category_name')
        ->join('product_categories', 'products.categories_id = product_categories.id' )
        ->orderBy('products.id', 'DESC');
        
        if ($paginate == null) {
            return $data->get();
        }else{
            return  $data->paginate($paginate, 'product');
        }
       
    }
}