<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class TransactionModel extends Model
{
    protected $table      = 'transactions';
    protected $primaryKey = 'id';
    protected $returnType     = 'object';
    protected $allowedFields = ['users_id', 'address','total_price','shipping_price	', 'status','payment','created_at', 'updated_at'];
    protected $useTimestamps = true;


    public function printStruk($id = null){
        $db = \Config\Database::connect();
        $data['transaction'] = $db->table('transactions')
        ->join('users', 'users.id = transactions.users_id')
        ->select('transactions.*, users.name')
        ->where('transactions.id', $id)
        ->get()
        ->getRow();
        
        $data['items'] = $db->table('transaction_items')
        ->join('products', 'products.id = transaction_items.products_id')
        ->select('transaction_items.*, products.name, products.price')
        ->where('transaction_items.transactions_id', $id)
        ->get()
        ->getResult();

        return $data;
    }
}