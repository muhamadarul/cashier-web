<?php namespace App\Models;
use CodeIgniter\Model;
 
class DashboardModel extends Model
{
     
    public function getDashboard()
    {   
        $data['member']  = $this->db->table('users')->countAll();
        $data['product']  = $this->db->table('products')->countAll();
        $data['sales']  = $this->db->table('transactions')->where('status', 'PAID')->countAllResults();
        $data['rec_product'] = $this->db->table('products')
                              ->select('products.*, product_categories.name as category_name')
                              ->join('product_categories', 'products.categories_id = product_categories.id' )
                              ->orderBy('products.id', 'DESC')->limit(5)->get()->getResultArray();
        $data['rec_trx'] = $this->db->table('transactions')
                        ->select('transactions.*, users.name')
                        ->join('users', 'transactions.users_id = users.id' )
                        ->where('status', 'PAID')->orderBy('transactions.id', 'DESC')
                        ->limit(5)->get()->getResultArray();
        return $data;  
    }
 }