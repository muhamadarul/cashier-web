<?= $this->extend('layouts/main-layout'); ?>

<?= $this->section('title') ?>
Category
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Category</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Category </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="mb-5">
          <a href="<?= base_url('product/category/create') ?>" class="btn btn-primary">Add data</a>
         </div>
        <!-- Row -->
        <div class="row">
          <div class="col-md-6">
            <?php if(session()->getFlashdata('success_msg')):?>
               <div class="mt-3 alert alert-success"><?= session()->getFlashdata('success_msg') ?></div>
            <?php endif;?>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Datas</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Category</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $nomor = 1;
                    foreach ($category as $row) :
                    ?>
                    <tr>
                      <td><?= $nomor++; ?>.</td>
                      <td><?= $row->name; ?></td>
                      <td>
                        <a href="<?= base_url('/product/category/'.$row->id.'/edit') ?>" class="btn btn-secondary">Edit</a>
                        <a href="#" href="#" data-href="<?= base_url('/product/category/'.$row->id.'/destroy') ?>" onclick="confirmToDelete(this)" class="btn btn-danger">Delete</a>
                      </td>
                    </tr>
                    <?php
                     endforeach;
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    <?= $pager->links('category','bootstrap_pagination'); ?>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      

  <?= $this->include('components/modal-delete'); ?>

<?= $this->endSection() ?>