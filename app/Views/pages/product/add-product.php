<?= $this->extend('layouts/main-layout'); ?>

<?= $this->section('title') ?>
Add Product 
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Product </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Row -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Add Product</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <?php if(isset($validation)):?>
                    <div class="mt-3 alert alert-danger"><?= $validation->listErrors() ?></div>
                <?php endif;?>
                <form action="<?= base_url('product/create') ?>" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mt-3">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" value="" required class="form-control">
                            </div>
                            <div class="mt-3">
                                <label for="price">Price</label>
                                <input type="number" id="price" name="price" value="" required class="form-control">
                            </div>
                    
                            <div class="mt-3">
                                <label for="description">Description</label>
                                <textarea name="description" class="form-control" id="description" cols="30" rows="10"></textarea>
                            </div>
                        
                        </div>
                        <div class="col-md-6">
                            <div class="mt-3">
                                    <label for="categories_id">Category</label>
                                    <select class="form-control" name="categories_id" id="categories_id">
                                        <option>-- Choose Category --</option>
                                        <?php foreach ($category as $row) : ?>
                                        <option value="<?= $row->id ?>"><?= $row->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <div class="mt-3">
                                <label for="tags">Tags</label>
                                <input type="text" id="tags" name="tags" value="" required class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="mt-3 d-flex justify-content-end">
                        <button class="btn btn-primary" type="submit">Create</button>
                    </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
<?= $this->endSection() ?>