<?= $this->extend('layouts/main-layout'); ?>

<?= $this->section('title') ?>
Product
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Product </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="mb-5">
          <a href="<?= base_url('product/create') ?>" class="btn btn-primary">Add data</a>
         </div>
        <!-- Row -->
        <div class="row">
          <div class="col-md-12">
            <?php if(session()->getFlashdata('success_msg')):?>
               <div class="mt-3 alert alert-success"><?= session()->getFlashdata('success_msg') ?></div>
            <?php endif;?>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Datas</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-responsive">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Name</th>
                      <th>Price</th>
                      <th>Description</th>
                      <th>Tags</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $nomor = 1;
                    foreach ($product as $row) :
                    ?>
                    <tr>
                      <td><?= $nomor++; ?>.</td>
                      <td><?= $row->name; ?></td>
                      <td><?= $row->price; ?></td>
                      <td><?= $row->description; ?></td>
                      <td><?= $row->tags; ?></td>
                      <td>
                        <a href="<?= base_url('product/'.$row->id.'/edit') ?>" class="btn btn-secondary">Edit</a>
                        <a href="<?= base_url('product/'.$row->id.'/gallery') ?>" class="btn btn-warning">Gallery Product</a>
                        <a href="#" href="#" data-href="<?= base_url('product/'.$row->id.'/destroy') ?>" onclick="confirmToDelete(this)" class="btn btn-danger">Delete</a>
                      </td>
                    </tr>
                    <?php
                     endforeach;
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    <?= $pager->links('product','bootstrap_pagination'); ?>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
<?= $this->include('components/modal-delete'); ?>

<?= $this->endSection() ?>