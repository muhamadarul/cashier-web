<?= $this->extend('layouts/main-layout'); ?>

<?= $this->section('title') ?>
Gallery Product
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Gallery Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Gallery Product </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="mb-5">
          <a href="<?= base_url('product/'.$products_id.'/gallery/create') ?>" class="btn btn-primary">Add data</a>
         </div>
        <!-- Row -->
        <div class="row">
          <div class="col-md-12">
            <?php if(session()->getFlashdata('success_msg')):?>
               <div class="mt-3 alert alert-success"><?= session()->getFlashdata('success_msg') ?></div>
            <?php endif;?>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Datas</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered table-responsive">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Name</th>
                      <th>Photo</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $nomor = 1;
                    foreach ($gallery as $row) :
                    ?>
                    <tr>
                      <td><?= $nomor++; ?>.</td>
                      <td><?= $row->product_name; ?></td>
                      <td><img src="<?= base_url('assets/images/'.$row->url); ?>" alt="" style="object-fit: contain; width:50px"></td>
                      <td>
                        <a href="#" href="#" data-href="<?= base_url('product/'.$row->id.'/gallery/destroy') ?>" onclick="confirmToDelete(this)" class="btn btn-danger">Delete</a>
                      </td>
                    </tr>
                    <?php
                     endforeach;
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                    <?= $pager->links('gallery','bootstrap_pagination'); ?>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
<?= $this->include('components/modal-delete'); ?>

<?= $this->endSection() ?>