<html>
    <head>
        <title>Cetak Nota <?= 'INVOICE'.$transaction->id ?></title>
        <link rel="stylesheet" href="<?= base_url('assets/css/print.css?time='. md5(time())) ?>">
    </head>
    <body class="struk" onload="printOut()">
        <section class="sheet">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>TOKO SEPATU TANAH BARU</td>
            </tr>
            <tr>
                <td>TANAH BARU HARJA MEKAR CIKARANG UTARA BEKASI</td>
            </tr>
            <tr>
                <td>Telp: 021-00000000</td>
            </tr>
        </table> 
        <?php
          echo 
          '<tr>
              <td align="left" class="txt-left">'. str_repeat("=", 38) . '</td>
          </tr>';
        ?>
        
        <table cellpadding="0" cellspacing="0" style="width:100%">
            <tr>
                <td align="left" class="txt-left">Nota&nbsp;</td>
                <td align="left" class="txt-left">:</td>
                <td align="left" class="txt-left">&nbsp;<?= 'INVOICE'.$transaction->id ?></td>
            </tr>
            <tr>
                <td align="left" class="txt-left">Kasir</td>
                <td align="left" class="txt-left">:</td>
                <td align="left" class="txt-left">&nbsp;<?= $transaction->name ?></td>
            </tr>
            <tr>
                <td align="left" class="txt-left">Tgl.&nbsp;</td>
                <td align="left" class="txt-left">:</td>
                <td align="left" class="txt-left">&nbsp;<?= $transaction->created_at ?></td>
            </tr>
        </table>
        <?php
            $tItem = 'Item'. str_repeat("&nbsp;", (13 - strlen('Item')));
            $tQty  = 'Qty'. str_repeat("&nbsp;", (6 - strlen('Qty')));
            $tHarga= str_repeat("&nbsp;", (9 - strlen('Harga'))).'Harga';
            $tTotal= str_repeat("&nbsp;", (10 - strlen('Total'))).'Total';
            $caption = $tItem. $tQty. $tHarga. $tTotal;

            echo    '<table cellpadding="0" cellspacing="0" style="width:100%">
                        <tr>
                            <td align="left" class="txt-left">'. $caption . '</td>
                        </tr>
                        <tr>
                            <td align="left" class="txt-left">'. str_repeat("=", 38) . '</td>
                        </tr>';
            if(!empty($items))
            {
                foreach($items as $k=>$v)
                {
                    $item = $v->name. str_repeat("&nbsp;", (38 - (strlen($v->name))));
                    echo '<tr>';
                        echo'<td align="left" class="txt-left">'.$item.'</td>';
                    echo '</tr>';

                    echo '<tr>';
                    $qty        = strval($v->quantity); 
                    $qty        = $qty  . str_repeat("&nbsp;", ( 13 - strlen($qty)));
                    $price        = strval($v->price);
                    $price      = str_repeat("&nbsp;", ( 9 - strlen($price)) ). $price;

                    $total      = $v->price * $v->quantity;
                    $lentotal   = strlen($total);
                    $total      = str_repeat("&nbsp;", ( 10 - $lentotal) ). $total;
                        echo'<td class="txt-left" align="left">'.$qty. $price. $total .'</td>';
                    
                    echo '</tr>';
                }

                echo '<tr><td>'. str_repeat('-', 38).'</td></tr>';

                //Sub Total
                $titleST = 'Sub&nbspTotal';
                $titleST = $titleST. str_repeat("&nbsp;", ( 19 - strlen($titleST)) );
                $ST      = $transaction->total_price;
                $ST      = str_repeat("&nbsp;", ( 23 - strlen($ST)) ). $ST;
                echo '<tr><td>'. $titleST. $ST.'</td></tr>';
                //Diskon
                $titleDs = 'Diskon';
                $titleDs = $titleDs. str_repeat("&nbsp;", ( 15 - strlen($titleDs)) );
                $Ds      = 0;
                $Ds      = str_repeat("&nbsp;", ( 23 - strlen($Ds)) ). $Ds;
                echo '<tr><td>'. $titleDs. $Ds.'</td></tr>';
                //PPN
                $titlePPn = 'PPN';
                $titlePPn = $titlePPn. str_repeat("&nbsp;", ( 15 - strlen($titlePPn)) );
                $PPn      = 0;
                $PPn      = str_repeat("&nbsp;", ( 23 - strlen($PPn)) ). $PPn;
                echo '<tr><td>'. $titlePPn. $PPn.'</td></tr>';


                //Grand Total
                $titleGT = 'Grand&nbspTotal';
                $titleGT = $titleGT. str_repeat("&nbsp;", ( 19 - strlen($titleGT)) );
                $GT      = $transaction->total_price;
                $GT      = str_repeat("&nbsp;", ( 23 - strlen($GT)) ). $GT;
                echo '<tr><td>'. $titleGT. $GT.'</td></tr>';
                
                // //Bayar
                // $titlePy = 'BAYAR';
                // $titlePy = $titlePy. str_repeat("&nbsp;", ( 15 - strlen($titlePy)) );
                // $Py      = Rupiah($d->pay, 2);
                // $Py      = str_repeat("&nbsp;", ( 23 - strlen($Py)) ). $Py;
                // echo '<tr><td>'. $titlePy. $Py.'</td></tr>';

                // $kembali= $d->payment_type == 'Angsuran' ? 0 : $d->pay - $d->grand_total;
                // //Kembali
                // $titleK = 'KEMBALI';
                // $titleK = $titleK. str_repeat("&nbsp;", ( 15 - strlen($titleK)) );
                // $Kb     = Rupiah(($kembali), 2);
                // $Kb      = str_repeat("&nbsp;", ( 23 - strlen($Kb)) ). $Kb;
                // echo '<tr><td>'. $titleK. $Kb.'</td></tr>';
                // echo '<tr><td>&nbsp;</td></tr>';

            }
            echo '</table>';

            $footer = 'Terima kasih atas kunjungan anda';
            $starSpace = ( 32 - strlen($footer) ) / 2;
            $starFooter = str_repeat('*', $starSpace+1);
            echo($starFooter. '&nbsp;'.$footer . '&nbsp;'. $starFooter."<br/><br/><br/><br/>");
            echo '<p>&nbsp;</p>';  
            
        ?>
        </section>
        <script>
            var lama = 1000;
            t = null;
            function printOut(){
                console.log('disini');
                window.print();
                t = setTimeout("self.close()",lama);
            }
</script>
    </body>
</html>