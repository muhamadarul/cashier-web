<?= $this->extend('layouts/main-layout'); ?>

<?= $this->section('title') ?>
New Order
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">New Order</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">New Order </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Row -->
        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Add Product</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="mb-3">
                    <label for="product">Product</label>
                    <select id='product' class="form-control">
                    </select>
                </div>
                <div class="mb-3">
                    <label for="qty">Qty Product</label>
                    <input type="number" min="0" class="form-control" value="" name="qty" id="qty">
                </div>
                <input type="hidden" name="product-val" id="product-val" value="0">
                <div class="mb-3">
                    <button id="add-product" class="btn btn-primary">Add Product</button>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Total Amount</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="d-flex justify-content-start align-items-center">
                            <div class="">
                                <h3>$</h3>
                            </div>
                            <div>
                                <h3 id="total-amount">0</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
              </div>
              <div class="col-12">
                <form action="<?= base_url('transaction/new-order') ?>" method="post">
                      <div id="list-input-order">

                      </div>
                      <button type="submit" id="submit-btn" class="btn btn-success">Process</button>
                  </form>
              </div>
            </div>
          </div>

          <!-- /.col -->
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Datas</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Subtotal</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="list-order">

                        </tbody>
                    </table>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->

  



      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      
  <script>
    var countItem = 0;
    $(function(){
        activeBtn()
        $("#product").select2({
        ajax: { 
          url: "<?= base_url('ajax/get-product')?>",
          type: "post",
          dataType: 'json',
          delay: 250,
          data: function (params) {
             return {
                searchTerm: params.term, // search term
             };
          },
          processResults: function (response) {
             return {
                results: response.data
             };
          },
          cache: true
        }
     });
    })

    $('#product').on('change', function () {
        $('#product-val').val(this.value)
        $('#qty').val(0)
    })

    $('#add-product').on('click', function () {
        var product = $('#product-val').val()
        var qty = $('#qty').val()

        if (qty === "" || qty === "0" || qty === 0) {
            $('#qty').addClass('is-invalid')
        }
        if (product === '' ) {
            $('#product').addClass('is-invalid')
        }


        if ((qty !== "" && qty !== "0" && qty !== 0 )&& product !== '') {

            var product_arr = product.split('^^^');
            var product_id =  product_arr[0];
            var product_name =  product_arr[1];
            var product_price =  parseInt(product_arr[2]);
            var subtotal = parseInt(product_price) * parseInt(qty);

            // check jika product sudah ada dalam cart
            // maka update qty dan subtotal-nya
            var count_elemet = $('#list-'+product_id).length;

            if (count_elemet > 0) {
                var c_subtotal = parseInt($('#subtotal_product-'+product_id).val());
                var c_qty = parseInt($('#qty_product-'+product_id).val());
                
                var n_subtotal = c_subtotal + subtotal;
                var n_qty = c_qty + parseInt(qty);

                //update ke input type
                $('#subtotal_product-'+product_id).val(n_subtotal)
                $('#qty_product-'+product_id).val(n_qty)
                
                //update ke table
                $('#t_subtotal-'+product_id).text('$ '+n_subtotal)
                $('#t_qty-'+product_id).text(n_qty)
            }else {
                $('#list-order').append(`
                    <tr id="list-${product_id}">
                        <td>${product_name}</td>
                        <td>$ ${product_price}</td>
                        <td id="t_qty-${product_id}">${qty}</td>
                        <td id="t_subtotal-${product_id}">$ ${subtotal}</td>
                        <td><button onclick="hapus(${product_id})" class="btn btn-danger">Delete</button></td>
                    </tr>
                `)
                $('#list-input-order').append(`
                    <div id="val-${product_id}">
                        <input type="hidden" name="product_id[${product_id}]" value="${product_id}" id="product_id-${product_id}">
                        <input type="hidden" name="qty_product[${product_id}]" value="${qty}" id="qty_product-${product_id}">
                        <input type="hidden" name="subtotal_product[${product_id}]" value="${subtotal}" id="subtotal_product-${product_id}">
                    </div>
                `)
                countItem++
                activeBtn()
            }
            
            updateAmout(type = 'add', subtotal)
        }
    })

    function updateAmout(type = 'add', amount = 0){
       var t_amount = parseInt($('#total-amount').text());
       if (type === 'add') {
            t_amount = t_amount + amount;     
       }else{
            t_amount = t_amount - amount;     
       }
       $('#total-amount').text(t_amount)
       $('#val_t_amount').val(t_amount)
    }

    function hapus(id){
        var c_subtotal = parseInt($('#subtotal_product-'+id).val())
        countItem--
        updateAmout(type = 'min', c_subtotal)   
        activeBtn()
        $('#val-'+id).remove()
        $('#list-'+id).remove()

    }

    function activeBtn(){
      if (countItem > 0) {
        $('#submit-btn').removeClass('d-none');
      }else{
        $('#submit-btn').addClass('d-none');
      }
    }
  </script>


<?= $this->endSection() ?>