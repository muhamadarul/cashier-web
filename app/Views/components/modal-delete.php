<div id="confirm-dialog" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h2 class="h2">Are you sure?</h2>
          <p>The data will be deleted and lost forever</p>
        </div>
        <div class="modal-footer">
          <form action="" id="delete-form" method="post">
            <button role="button" id="delete-button" class="btn btn-danger">Delete</button>
          </form>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>

<script>
function confirmToDelete(el){
    $("#delete-form").attr("action", el.dataset.href);
    $("#confirm-dialog").modal('show');
}
</script>