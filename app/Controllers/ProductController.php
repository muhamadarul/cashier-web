<?php

namespace App\Controllers;

use App\Models\ProductModel;
use App\Models\CategoryProductModel;


class ProductController extends BaseController
{
    public function index()
    {
        $model = new ProductModel();
        $data['product'] = $model->productWithCategory(5);
        $data['pager'] = $model->pager;
        return view('pages/product/product', $data);
    }

    public function create() {
        $data['category'] = $this->getCategory();
        return view('pages/product/add-product', $data);
    }

    public function save(){
        helper(['form']);
        $session = session();

        $rules = [
            'name'          => 'required|min_length[3]|max_length[50]',
            'price'          => 'required|numeric',
            'description'          => 'required|min_length[3]',
            'categories_id'          => 'required',
        ];

        if($this->validate($rules)){
            $model = new ProductModel();
            $data = [
                'name'     => $this->request->getVar('name'),
                'price'     => $this->request->getVar('price'),
                'description'     => $this->request->getVar('description'),
                'categories_id'     => $this->request->getVar('categories_id'),
                'tags'     => $this->request->getVar('tags'),
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ];
            $model->save($data);
            $session->setFlashdata('success_msg', 'Data added succesfully');
            return redirect()->to('/product');
        }else{
            $data['validation'] = $this->validator;
            $data['category'] = $this->getCategory();
            echo view('pages/product/add-product', $data);
        }
    }

    public function update($id){
        helper(['form']);
        $session = session();

        $rules = [
            'name'          => 'required|min_length[3]|max_length[50]',
            'price'          => 'required|numeric',
            'description'          => 'required|min_length[3]',
            'categories_id'          => 'required',
        ];

        if($this->validate($rules)){
            $model = new ProductModel();
            $data = [
                'name'     => $this->request->getVar('name'),
                'price'     => $this->request->getVar('price'),
                'description'     => $this->request->getVar('description'),
                'categories_id'     => $this->request->getVar('categories_id'),
                'tags'     => $this->request->getVar('tags'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ];
            $model->update($id, $data);
            $session->setFlashdata('success_msg', 'Data updated succesfully');
            return redirect()->to('/product');
        }else{
            $data['validation'] = $this->validator;
            $data['category'] = $this->getCategory();
            $data['product'] = $this->getDetailProduct($id);
            echo view('pages/product/add-product', $data);

        }
    }
    
    public function destroy($id){
        $session = session();

        $model = new ProductModel();
        $model->delete($id);

        $session->setFlashdata('success_msg', 'Data deleted succesfully');
        return redirect()->to('/product');
    }

    public function edit($id) {
        $data['product'] = $this->getDetailProduct($id);
        $data['category'] = $this->getCategory();
        // dd($data);
        return view('pages/product/edit-product', $data);
    }

    public function getCategory(){
        $model = new CategoryProductModel();
        $data =  $model->findAll();
        return $data;
    }

    public function getDetailProduct($id){
        $model = new ProductModel();
        $data =  $model->where('id', $id)->first();
        return $data;
    }

}
