<?php

namespace App\Controllers;

use App\Models\CategoryProductModel;

class CategoryProductController extends BaseController
{
    public function index()
    {
        $model = new CategoryProductModel();
        $data['category'] = $model->paginate(5,'category');
        $data['pager'] = $model->pager;
        return view('pages/category_product/category', $data);
    }

    public function create() {
        return view('pages/category_product/add-category');
    }

    public function save(){
        helper(['form']);
        $session = session();
        

        $rules = [
            'category'          => 'required|min_length[3]|max_length[50]'
        ];

        if($this->validate($rules)){
            $model = new CategoryProductModel();
            $data = [
                'name'     => $this->request->getVar('category'),
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ];
            $model->save($data);
            $session->setFlashdata('success_msg', 'Data added succesfully');
            return redirect()->to('/product/category');
        }else{
            $data['validation'] = $this->validator;
            echo view('pages/category_product/add-category', $data);
        }
    }

    public function edit($id) {
        $model = new CategoryProductModel();
        $data['category'] =  $model->where('id', $id)->first();
        return view('pages/category_product/edit-category', $data);
    }

    public function update($id){
        helper(['form']);
        $session = session();

        $rules = [
            'category'          => 'required|min_length[3]|max_length[50]'
        ];

        if($this->validate($rules)){
            $model = new CategoryProductModel();
            $data = [
                'name'     => $this->request->getVar('category'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ];
            $model->update($id, $data);
            $session->setFlashdata('success_msg', 'Data updated succesfully');
            return redirect()->to('/product/category');
        }else{
            $data['validation'] = $this->validator;
            echo view('pages/category_product/edit-category', $data);
        }
    }

    public function destroy($id){
        $session = session();

        $model = new CategoryProductModel();
        $model->delete($id);

        $session->setFlashdata('success_msg', 'Data deleted succesfully');
        return redirect()->to('/product/category');
    }
}
