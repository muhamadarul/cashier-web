<?php

namespace App\Controllers;
use App\Models\DashboardModel;

class DashboardController extends BaseController
{
    public function index()
    {
        $dashboard_m = new DashboardModel();
        $data['dashboard'] = $dashboard_m->getDashboard();
        // dd($data['dashboard']['rec_trx']);
        return view('pages/dashboard/dashboard', $data);
    }
}
