<?php

namespace App\Controllers;

use App\Models\GalleryProductModel;

class GalleryProductController extends BaseController
{
    public function index($id)
    {
        $m_gallery = new GalleryProductModel();
        $data['gallery'] = $m_gallery->galleryWithProduct($id, 5);
        $data['pager'] = $m_gallery->pager;
        $data['products_id'] = $id;
        return view('pages/gallery_product/gallery', $data);
    }

    public function create($id){
        $data['products_id'] = $id;
        return view('pages/gallery_product/add-gallery', $data);
    }

    public function save($id){
        helper(['form']);
        $session = session();

        $rules = [
            'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload,4096]'
        ];

        if($this->validate($rules)){
           
            $upload = $this->request->getFile('file_upload');
            $fileName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../public/assets/images/', $fileName);

            $model = new GalleryProductModel();
            $data = [
                'url'     => $fileName,
                'products_id'     => $this->request->getVar('products_id'),
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ];
            $model->save($data);
            $session->setFlashdata('success_msg', 'Data uploaded succesfully');
            return redirect()->to('product/'.$id.'/gallery');

        }else{
            $data['products_id'] = $id;
            $data['validation'] = $this->validator;
            return view('pages/gallery_product/add-gallery', $data);
        }

    }

    public function destroy($id){
        $session = session();

        $model = new GalleryProductModel();

        $data = $model->find($id);

        if (file_exists("assets/images/".$data->url)) {
            unlink("assets/images/".$data->url);
        }

        $model->delete($id);

        $session->setFlashdata('success_msg', 'Data deleted succesfully');
        return redirect()->to('product/'.$data->products_id.'/gallery');
    }

}
