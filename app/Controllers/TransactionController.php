<?php

namespace App\Controllers;

use App\Models\ProductModel;
use App\Models\TransactionDetailModel;
use App\Models\TransactionModel;

class TransactionController extends BaseController
{
    public function index()
    {
        return view('pages/transaction/new-order');
    }

    public function ajaxGetProduct(){
        $request = service('request');
        $postData = $request->getPost();
    
        $response = array();
    
        // Read new token and assign in $response['token']
        $response['token'] = csrf_hash();
    
        if(!isset($postData['searchTerm'])){
            // Fetch record
            $model = new ProductModel();
            $productList = $model->select('id,name,price')
                ->orderBy('name')
                ->findAll(5);
        }else{
            $searchTerm = $postData['searchTerm'];
    
            // Fetch record
            $model = new ProductModel();
            $productList = $model->select('id,name,price')
                ->like('name',$searchTerm)
                ->orderBy('name')
                ->findAll(5);
        } 
    
        $data = array();
        foreach($productList as $product){
            $data[] = array(
                "id" => $product->id.'^^^'.$product->name.'^^^'.$product->price,
                "text" => $product->name,
            );
        }
    
        $response['data'] = $data;
    
        return $this->response->setJSON($response);
    }


    public function processTrx(){
        $session = session();
        $users_id = $session->user_id;
       
        $items = $this->request->getVar('qty_product');

        $total_price = 0;

        foreach($items as $key => $item) {

            $products_id = (int)$key;
            $qty = (int)$item;


            $subtotal = 0;
            $m_product = new ProductModel();
            $price = $m_product->where('id', $products_id)->first()->price;
            
            $subtotal =  $price * $qty;

            $data[] = [
                'products_id' => $products_id,
                'qty' => $qty,
            ];
            
            $total_price += $subtotal;
        }


        $m_trx = new TransactionModel();
        $id = $m_trx->insert([
            'users_id' => $users_id,
            'address' => '-',
            'total_price' => $total_price,
            'shipping_price	' => 0,
            'status' => 'PAID',
            'payment' => 'MANUAL',
            'created_at'     => date('Y-m-d H:i:s'),
            'updated_at'     => date('Y-m-d H:i:s'),
        ]);

        foreach ($data as $row) {
            $m_trx_detail = new TransactionDetailModel();
            $m_trx_detail->insert([
                'users_id' => $users_id,
                'transactions_id' => $id, 
                'products_id' => $row['products_id'],
                'quantity' => $row['qty'],
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ]);
        }
        
        $session->setFlashdata('success_msg', 'Transaction created succesfully');
        return redirect()->to('transaction/'.$id.'/print-struk');
    }

    public function printStruk($id){
        $model = new TransactionModel();
        $data = $model->printStruk($id);
        return view('pages/transaction/print', $data);
    }
}
