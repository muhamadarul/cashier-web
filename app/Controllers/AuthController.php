<?php

namespace App\Controllers;
use App\Models\UserModel;

class AuthController extends BaseController
{
    public function loginPage()
    {
        helper(['form']);
        return view('auth/login');
    }

    public function loginAct(){
        $session = session();
        $model = new UserModel();

        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        $check = $model->where('email', $email)->first();

        if ($check) {
            $password_user = $check['password'];
            $verify = password_verify($password, $password_user);
            if ($verify) {
                $session_data = [
                    'user_id' => $check['id'],
                    'name' => $check['name'],
                    'email' => $check['email'],
                    'is_login' => TRUE,
                ];

                $session->set($session_data);
                return redirect()->to('dashboard');
            }else{
                $session->setFlashdata('msg', 'Wrong Password');
                return redirect()->to('/');
            }
        }else{
            $session->setFlashdata('msg', 'Email not Found');
            return redirect()->to('/');
        }

    }

    public function registerPage()
    {
        return view('auth/register');
    }

    public function registerAct(){
        //include helper form
        helper(['form']);
        //set rules validation form
        $rules = [
            'name'          => 'required|min_length[3]|max_length[20]',
            'email'         => 'required|min_length[6]|max_length[50]|valid_email|is_unique[users.email]',
            'password'      => 'required|min_length[6]|max_length[200]',
            'password_confirmation'  => 'matches[password]'
        ];
        // dd( $this->request->getVar('password_confirmation'));
        if($this->validate($rules)){
            $model = new UserModel();
            $data = [
                'name'     => $this->request->getVar('name'),
                'email'    => $this->request->getVar('email'),
                'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ];
            $model->save($data);
            return redirect()->to('/');
        }else{
            $data['validation'] = $this->validator;
            echo view('auth/register', $data);
        }
    }

}
